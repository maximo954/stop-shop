import { ShopStopPage } from './app.po';

describe('shop-stop App', () => {
  let page: ShopStopPage;

  beforeEach(() => {
    page = new ShopStopPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
